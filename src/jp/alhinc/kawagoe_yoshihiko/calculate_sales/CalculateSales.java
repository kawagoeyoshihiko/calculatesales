package jp.alhinc.kawagoe_yoshihiko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		try {
			Map<String, String> map = new HashMap<String, String>();//支店コードをキー、支店名をプットする
			Map<String, Integer> mapKingaku = new HashMap<String, Integer>();

			String[] shitenFile = null;//shitenFileの内容を売上ファイル読み込み時に出力するために記載。
			BufferedReader br = null;
			try {
				Path filePath = Paths.get(args[0], "branch.lst");
				if (!Files.exists(filePath)) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				File file = new File(args[0], "branch.lst");//ブランチリストを呼び出す。
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					shitenFile = line.split(",", 0);
					if (!shitenFile[0].matches("^[0-9]{3}") || shitenFile.length != 2) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					map.put(shitenFile[0], shitenFile[1]);
					mapKingaku.put(shitenFile[0], 0);
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				br.close();
			}
			Map<String, Long> mapUriagekingaku = new HashMap<String, Long>();

			//以下ファイルフィルタ（連番チェックのため）
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String name) {
					return name.matches("^[0-9]{8}.rcd$");
				}
			};

			//↓今からrcdファイルを全部読み込む
			File dir = new File(args[0]);
			File[] list = dir.listFiles(filter);
			Arrays.sort(list);
			String firstList = list[0].getName().substring(0, 8);
			String lastList = list[list.length - 1].getName().substring(0, 8);
			int firstList1 = Integer.parseInt(firstList);
			int lastList1 = Integer.parseInt(lastList);
			int listcheck = firstList1 + lastList1 - list.length;
			if (listcheck != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			for (int i = 0; i < list.length; i++) {

				BufferedReader br1 = null;
				try {
					File file = new File(args[0], list[i].getName());
					FileReader fr = new FileReader(file);
					br1 = new BufferedReader(fr);
					//↓1,2行目を読み込む
					String line = br1.readLine();
					String lineNext = br1.readLine();
					String lineThird = br1.readLine();
					if (lineThird != null) {
						System.out.println(list[i].getName() + "のフォーマットが不正です");
						return;
					}
					//↓売上金額をLong型に変換する。
					Long line2 = Long.parseLong(lineNext);//売上金額
					if (map.containsKey(line)) {
						Long total = line2 + mapKingaku.get(shitenFile[0]);
						int totallen = String.valueOf(total).length();
						if (totallen > 10) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
						mapUriagekingaku.put(line, total);
					} else {
						System.out.println(list[i].getName() + "の支店コードが不正です");
						return;
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					br1.close();
				}

				BufferedWriter bw = null;
				try {
					File fileOut = new File(args[0], "branch.out");
					FileWriter filewriter = new FileWriter(fileOut);
					bw = new BufferedWriter(filewriter);
					for (Map.Entry<String, String> entry : map.entrySet()) {
						bw.write(entry.getKey() + "," + entry.getValue() + "," + mapUriagekingaku.get(entry.getKey()));
						bw.newLine();
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					bw.close();
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
